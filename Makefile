# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/12/07 01:27:33 by tiboitel          #+#    #+#              #
#    Updated: 2014/12/07 08:54:33 by tiboitel         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME 		 = 	chip8
SRCS 		 = 	main.c \
				chip8.c \
				roms.c \
				chip8_debug.c

INCLUDES	 =	./includes
SRCSPATH 	 =	./srcs/
LIBFTPATH 	 =  ./libft/

CC			 = gcc
CFLAGS		 = -Wall -Werror -Wextra -g
INCLUDES_O	 = -I $(LIBFTPATH) -I $(INCLUDES)
INCLUDES_C	 = -L $(LIBFTPATH) -lft

SRC			 = $(addprefix $(SRCSPATH), $(SRCS))
OBJS		 = $(SRC:.c=.o)

all:		$(NAME)

$(NAME):	$(OBJS)
			$(CC) -o $(NAME) $(OBJS) $(CFLAGS) $(INCLUDES_C) -g3

%.o: %.c libft/libft.a
			$(CC) -o $@ $(CFLAGS) $(INCLUDES_O) -c $<

libft/libft.a:
			make -C $(LIBFTPATH)

clean:
			make -C $(LIBFTPATH) clean
			rm -rf $(OBJS)

fclean: 	clean
			make -C $(LIBFTPATH) fclean
			rm -rf $(NAME)

re: fclean all

.PHONY: clean fclean re
			
