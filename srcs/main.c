/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/07 06:04:58 by tiboitel          #+#    #+#             */
/*   Updated: 2014/12/07 08:36:23 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "chip8.h"

int main(int argc, char **argv)
{
	t_chip8		*chip;
	int			running;

	if ((chip = ch_init()) == NULL)
	{
		ft_putendl_fd("Unable to intialize chip8.", 2);
		return (-1);
	}
	else
	{
		ft_putendl("[Chip 8] init...");
		ft_putendl("[Chip 8] Running !");
		ch_loadrom("roms/PONG", chip);
		running = 1;
		while (running)
		{
			running = chip->ch_emulatecycle(chip);
		}
		chip->ch_free(chip);
		ft_putendl("Chip 8 was freed.");
	}
	(void)argv;
	argc++;
	return (0);
}
