/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   roms.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/07 08:17:40 by tiboitel          #+#    #+#             */
/*   Updated: 2014/12/07 08:35:50 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "chip8.h"

int		ch_loadrom(const char *path, t_chip8 *chip)
{
	int		fd;
	int		bytes_read;
	char	*roms;
	
	bytes_read = 0;
	if ((roms = (char *)ft_memalloc(sizeof(char) * 0xFFF)) == NULL)
		return (-1);
	if ((fd = open(path, O_RDONLY)) < 0)
		return (-1);
	if ((bytes_read = read(fd, roms, 0xFFF)) < 0)
		return (-1);	
	close(fd);
	if (!roms || !*roms)
		return (-1);
	ft_memcpy(chip->memory + 0x200, roms, bytes_read);
	free(roms);
	return (0);
}
