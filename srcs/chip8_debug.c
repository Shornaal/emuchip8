/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chip8_debug.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/07 07:29:55 by tiboitel          #+#    #+#             */
/*   Updated: 2014/12/07 07:38:14 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "chip8.h"

void	ch_debugprint(t_chip8 *chip)
{
	if (!chip)
		return ;
	ft_putstr("[Debug] I:");
	ft_putnbr(chip->i);
	ft_putstr(" Program Counter:");
	ft_putnbr(chip->pc);
	ft_putstr(" Opcode:");
	ft_putnbr(chip->opcode);
	ft_putchar('\n');
}
