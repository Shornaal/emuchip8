/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chip8.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/07 06:21:29 by tiboitel          #+#    #+#             */
/*   Updated: 2014/12/07 09:14:30 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "chip8.h"

t_chip8		*ch_init()
{
	t_chip8		*chip;

	if ((chip = (t_chip8 *)ft_memalloc(sizeof(t_chip8))) == NULL)
		return (NULL);
	// Counter start at address 512.
	chip->pc = 0x200;
	chip->opcode = 0;
	chip->i = 0;
	chip->sp = 0;
	chip->delay_timer = 0;
	chip->delay_sound = 0;
	if ((chip->memory = (unsigned char *)ft_memalloc(sizeof(unsigned char)
					* 4096)) == NULL)
		return (NULL);
	ft_bzero(chip->memory, 4096);
	if ((chip->gfx = (unsigned char *)ft_memalloc(sizeof(unsigned char) 
					* (64 * 32))) == NULL)
		return (NULL);
	ft_bzero(chip->gfx, (64 * 32));
	if ((chip->c_register = (unsigned char *)ft_memalloc(sizeof(unsigned char)
				* 16)) == NULL)
		return (NULL);
	ft_bzero(chip->c_register, 16);
	if ((chip->stack = (unsigned int *)ft_memalloc(sizeof(int) * 16)) == NULL)
   		return (NULL);
	chip->ch_free = ch_free;
	chip->ch_emulatecycle = ch_emulatecycle;
	return (chip);
	// Add fonset here.
}

int		ch_emulatecycle(t_chip8 *chip)
{
	// Get opcode.
	chip->opcode = chip->memory[chip->pc] << 8 | chip->memory[chip->pc + 1];
	if ((chip->opcode & 0xF000)
	{
		if ((chip->opcode & 0xF000) == 0xA000))
		{
			// Delete after debug.
			ft_putendl("Call instruction: 0xA000.");
			ch_debugprint(chip);
			chip->i = chip->opcode & 0x0FFF;
			chip->pc += 2;
			ch_debugprint(chip);
			ft_putendl("End of instruction.");
		}
		if ((chip->opcode & 0xF000) == 0xB000)
		{
			chip->pc = (chip->opcode & 0x0FFF) + chip->c_register[0];
			chip->pc += 2;
		}
	}
	else
	{	
		/*if (chip->pc > 510 && chip->pc < 524)
		//{
			ft_putstr("Unknown opcode: ");
			ft_putnbr(chip->opcode);
			ft_putstr(" |  Memory address: ");
			// Ajouter une fonction hextoint & itohex
			ft_putnbr(chip->pc);
			ft_putstr(" value: ");
			ft_putnbr(chip->memory[chip->pc]);
			ft_putchar('\n');
		} */
		chip->pc += 2;
	}
	if (chip->delay_timer > 0)
		chip->delay_timer--;
	if (chip->delay_sound > 0)
	{
		if (chip->delay_sound == 1)
			ft_putendl("Beep.");
		chip->delay_sound--;
	}
	if (chip->pc == 4096)
		chip->pc = 0x200;
	return (1);
}

void	ch_free(t_chip8 *chip)
{
	if (chip)
	{
		chip->pc = 0;
		chip->opcode = 0;
		chip->i = 0;
		chip->sp = 0;
		chip->delay_timer = 0;
		chip->delay_sound = 0;
		free(chip->memory);
		chip->memory = NULL;
		free(chip->gfx);
		chip->gfx = NULL;
		free(chip->c_register);
		chip->c_register = NULL;
		free(chip->stack);
		chip->stack = NULL;
		free(chip);	
		chip = NULL;
	}
}
