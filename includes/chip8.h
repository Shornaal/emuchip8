/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chip8.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/07 06:05:37 by tiboitel          #+#    #+#             */
/*   Updated: 2014/12/07 08:32:46 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_CHIP8_H
# define FT_CHIP8_H

#include <libft.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

typedef struct		s_chip8
{
	// Two bytes long opcode
	unsigned int	opcode;
	// 4K memory
	unsigned char	*memory;
	// 16 entry register
	unsigned char	*c_register;
	// index & program counter
	unsigned int 	i;
	unsigned int	pc;
	// Screen on 2048px (64 * 32)
	unsigned char   *gfx;
	// Delay timer 60Hz
	unsigned char	delay_timer;
	unsigned char	delay_sound;
	// for memory previous addess of jump instruction 16lvl stack
	//  and stck index;
	unsigned int	*stack;
	unsigned int	sp;
	// Add here pointer functions
	void			(* ch_free)(struct s_chip8 *chip);
	int				(* ch_emulatecycle)(struct s_chip8 *chip);
}					t_chip8;

/* SETUP */
int			setupgrahics();
int			setupinput();
/* Chip 8 */
t_chip8		*ch_init();
int			ch_loadrom(const char *path, t_chip8 *chip);
int			ch_emulatecycle(t_chip8 *chip);
void		ch_free(t_chip8 *chip);
// Delete after debug.
void		ch_debugprint(t_chip8 *chip);
#endif
